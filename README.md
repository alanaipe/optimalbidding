## Optimal Bidding

This project was done as a part of Inter IIT tech meet 2018. Out of 21 IITs who participated in the competition, this algorithm secured first runnerup position.

### Problem Statement:

Liberalization of sale of electricity has led to competitive electricity markets. Generators and consumers can trade electricity at prices agreeable to both by bidding in the market, which is facilitated by market operators. 
In that context, problem statement is as follows:
�Devise an algorithm or a system which minimizes the total expenditure, spent for electricity, by a given community using the forecast of the day ahead demand, solar output and market price.�

### Methodology - An Overview
The algorithm attains the objective through two phases, as mentioned below:
- Optimizing day-ahead prediction, making them as closer to real values as possible. This is made possible by using signal processing and Machine Learning techniques.
- Using the above obtained optimized values, redistribution of demand is done, abiding by the constraints, so as to minimize the total expenditure of the community for n-days.


#### Solar Output Optimization
In validation dataset, the algorithm,given below, was able to optimize solar output for all days, with an average decrease in mean square error of 57.3%.
![flowchart](readme_resources/solaroptimization.PNG)

#### Demand and Price Optimization
![flowchart](readme_resources/demandprice.PNG) 

#### Expenditure minimization
This phase aims at  redistribution of demand between bidding blocks, following the steps given below:
- Find actual demand of the community which cannot be satisfied by solar power plant.
For each day, 
1)Find the highest price block which has not yet taken part in redistribution.
2)Until saturation of a constraint, satisfy its demand using excess solar energy available in previous blocks.
3)Until saturation of a constraint, satisfy remaining demand using lowest priced previous blocks.
